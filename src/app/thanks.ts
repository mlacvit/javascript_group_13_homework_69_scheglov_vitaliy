import { Component } from '@angular/core';

@Component({
  selector: 'app-thanks',
  template: `<div><h1>Your registration is accepted, thank you!</h1>
    <a routerLink="/user">go to all users</a>
    <br>
    <a routerLink="/">go to new registration</a></div>`,
  styles: [`
    div{
      width: 100%;
      text-align: center;
      padding-top: 300px;
    }
    h1 {
      color: green;
      width: 100%;
    }
    a{
      color: darkslateblue;
      font-size: 20px;
    }
  `]
})


export class Thanks {}
