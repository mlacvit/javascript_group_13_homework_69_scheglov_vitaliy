import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive } from '@angular/core';

export const phoneValidator = (): ValidatorFn => {
  return (control: AbstractControl): ValidationErrors | null => {
    const phoneFormat = /[+\\996 999 999999]/.test(control.value);
    if (phoneFormat){
      return null
    }
    return {phone: true};
  }
}

@Directive({
  selector: '[appPhone]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: PhoneValidatorDirective,
    multi: true
  }]
})
export class PhoneValidatorDirective implements Validator{
  validate(control: AbstractControl): ValidationErrors | null {
    return phoneValidator()(control);
  }
}
