import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterModel } from './register.model';
import { map, Subject } from 'rxjs';

@Injectable()
export class RegisterServices {
  regAll: RegisterModel[] | null = null;
  regChange = new Subject<RegisterModel[]>();
  regFetch = new Subject<boolean>();
  regDeleteUpLoading = new Subject<boolean>();

  constructor(private http: HttpClient) {}

  getUser() {
    this.regFetch.next(true);
    this.http.get<{ [id: string]: RegisterModel }>('https://mlacvit-10af9-default-rtdb.firebaseio.com/registration.json')
      .pipe(map(result => {
        if (result === null || undefined) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new RegisterModel(
            id,
            data.name,
            data.lastname,
            data.secondName,
            data.job,
            data.phone,
            data.gen,
            data.size,
            data.comment,
            data.skills,
            data.level,
          );
        });
      }))
      .subscribe(reg => {
        this.regAll = reg;
        this.regChange.next(this.regAll.slice());
        this.regFetch.next(false);
      },error => {
        this.regFetch.next(false);
      });
  }

  deleteUser(id: string){
    this.regDeleteUpLoading.next(true);
    this.http.delete(`https://mlacvit-10af9-default-rtdb.firebaseio.com/registration/${id}.json`).subscribe();
    this.regDeleteUpLoading.next(false);
    this.getUser()
  }

}
