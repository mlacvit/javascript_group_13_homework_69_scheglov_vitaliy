export class RegisterModel {
  constructor(
    public id: string,
    public name: string,
    public lastname: string,
    public secondName: string,
    public job: string,
    public phone: string,
    public gen: string,
    public size: string,
    public comment: string,
    public skills: string,
    public level: string,
  ) {}
}
