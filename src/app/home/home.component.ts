import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { RegisterModel } from '../register.model';
import { tap } from 'rxjs';
import { phoneValidator } from '../validate-phone.directive';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  preloader = false;
  formGroupReg!: FormGroup;

  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit(): void {
    this.formGroupReg = new FormGroup({
      name: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      secondName: new FormControl('', Validators.required),
      job: new FormControl('', Validators.required),
      skills: new FormArray([]),
      level: new FormArray([]),
      phone: new FormControl('', [
        Validators.required,
        phoneValidator,
      ]),
      gen: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      comment: new FormControl('', [
        Validators.required,
        Validators.maxLength(300),
      ]),
    });
  }

  sendRegist() {
    this.preloader = true;
    const id = '';
    const body = new RegisterModel(
      id,
      this.formGroupReg.value.name,
      this.formGroupReg.value.lastname,
      this.formGroupReg.value.secondName,
      this.formGroupReg.value.job,
      this.formGroupReg.value.phone,
      this.formGroupReg.value.gen,
      this.formGroupReg.value.size,
      this.formGroupReg.value.comment,
      this.formGroupReg.value.skills,
      this.formGroupReg.value.level,
    );

    this.http.post(`https://mlacvit-10af9-default-rtdb.firebaseio.com/registration.json`, body).pipe(
      tap(() => {
        this.preloader = false;
      }, error => {
        this.preloader = false;
      })
    ).subscribe()
    return this.router.navigate(['/thanks']);
  };

  fieldError(fieldName: string, errorField: string) {
    const field = this.formGroupReg.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorField]);
  }

  addSkill() {
    const skill = <FormArray>this.formGroupReg.get('skills');
    const level = <FormArray>this.formGroupReg.get('level');
    const skillControl = new FormControl('', Validators.required);
    const levelControl = new FormControl('', Validators.required);
    skill.push(skillControl);
    level.push(levelControl);
  }

  getSkill() {
    const skills = <FormArray>this.formGroupReg.get('skills');
    return skills.controls;
  }

  getLevel() {
    const levels = <FormArray>this.formGroupReg.get('level');
    return levels.controls;
  }

}
