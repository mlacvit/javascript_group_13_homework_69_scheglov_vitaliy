import { Component, OnDestroy, OnInit } from '@angular/core';
import { RegisterModel } from '../register.model';
import { Subscription } from 'rxjs';
import { RegisterServices } from '../register.services';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  regAll: RegisterModel[] | null = null;
  regChangeSubscriptions!: Subscription;
  regFetchingSubscriptions!: Subscription;
  isFetching: boolean = false;
  regFetchingSubscriptionsDelete!: Subscription;
  isFetchingDelete = false;

  constructor(public service: RegisterServices) { }

  ngOnInit(): void {
    this.service.getUser();
    this.regChangeSubscriptions = this.service.regChange.subscribe((reg: RegisterModel[]) => {
      this.regAll = reg;
    })
    this.regFetchingSubscriptions = this.service.regFetch.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    })
    this.regFetchingSubscriptionsDelete = this.service.regDeleteUpLoading.subscribe((isFetching: boolean) => {
      this.isFetchingDelete = isFetching;
      this.service.getUser();
    })
  }

  ngOnDestroy(): void {
    this.regChangeSubscriptions.unsubscribe();
    this.regFetchingSubscriptions.unsubscribe();
    this.regFetchingSubscriptionsDelete.unsubscribe();
  }
}
