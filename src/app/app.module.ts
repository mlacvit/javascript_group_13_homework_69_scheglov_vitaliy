import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Thanks } from './thanks';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PhoneValidatorDirective } from './validate-phone.directive';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { RegisterServices } from './register.services';


@NgModule({
  declarations: [
    AppComponent,
    PhoneValidatorDirective,
    Thanks,
    HomeComponent,
    UserComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [RegisterServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
